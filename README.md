# Hi there! :wave:

I'm Michael. Welcome to my GitLab.

I work on computational fluid dynamics :1234: :ocean: to study turbulence :cyclone: in engineering applications :airplane: :rocket:.
